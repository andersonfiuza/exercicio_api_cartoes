package br.com.itau.Model;


import javax.persistence.*;
import java.util.List;

@Entity
public class Cartao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

     private String numero;
//    private int cvv;
//    private String datavalidade;
//    private String bandeira;
//    private String variante;

    private Boolean ativo;

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
    public Cartao() {
    }

    public List<Pagamento> getPagamento() {
        return pagamento;
    }

    public void setPagamento(List<Pagamento> pagamento) {
        this.pagamento = pagamento;
    }

    @OneToMany
    public List<Pagamento> pagamento;


    @ManyToOne
    private Cliente cliente;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }
//
//    public int getCvv() {
//        return cvv;
//    }
//
//    public void setCvv(int cvv) {
//        this.cvv = cvv;
//    }
//
//    public String getDatavalidade() {
//        return datavalidade;
//    }
//
//    public void setDatavalidade(String datavalidade) {
//        this.datavalidade = datavalidade;
//    }
//
//    public String getBandeira() {
//        return bandeira;
//    }
//
//    public void setBandeira(String bandeira) {
//        this.bandeira = bandeira;
//    }
//
//    public String getVariante() {
//        return variante;
//    }
//
//    public void setVariante(String variante) {
//        this.variante = variante;
//    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
