package br.com.itau.DTO;

import br.com.itau.Model.Pagamento;

public class PagamentoEntradaDTO {


    private int cartao_id;
    private double valor;
    private  String descricao;

    public int getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(int cartao_id) {
        this.cartao_id = cartao_id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }


    public Pagamento converterParaPagamento (PagamentoEntradaDTO pagamentoEntradaDTO){
        Pagamento pagamento = new Pagamento();

        pagamento.setDescricao(pagamentoEntradaDTO.getDescricao());
        pagamento.setValor(pagamentoEntradaDTO.getValor());
        return pagamento;

    }

}
