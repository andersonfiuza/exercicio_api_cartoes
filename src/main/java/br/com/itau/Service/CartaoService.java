package br.com.itau.Service;


import br.com.itau.DTO.CartaoBloqueadoDTO;
import br.com.itau.DTO.CartaoDTO;
import br.com.itau.DTO.CartaoSaidaDTO;
import br.com.itau.Model.Cartao;
import br.com.itau.Model.Cliente;
import br.com.itau.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.security.krb5.internal.CredentialsUtil;

import java.util.Optional;

@Service
public class CartaoService {


    @Autowired
    CartaoRepository cartaoRepository;

    @Autowired
    ClienteService clienteService;


    public CartaoSaidaDTO solicitaNovoCartao(CartaoDTO cartaoDTO) {

        Cliente cliente = clienteService.buscarClientePeloId(cartaoDTO.getClienteId());


        Cartao cartao = new Cartao();
        cartao.setNumero(cartaoDTO.getNumero());
        cartao.setCliente(cliente);
        cartao.setAtivo(false);


        cartao =  cartaoRepository.save(cartao);

        return  new CartaoSaidaDTO(cartao);

    }


    //Ativa /Desativa o cartão.

    public Cartao ativaCartaoCliente(String numero, Boolean status) {

        Cartao cartao = buscarCartaoPeloNumero(numero);

        cartao.setAtivo(status);

        return cartaoRepository.save(cartao);

    }


    //Busca Cartao do cliente pelo numero.
    public Cartao buscarCartaoPeloNumero(String numero) {

        Optional<Cartao> cartaoOptional = cartaoRepository.findFirstByNumero(numero);

        if (cartaoOptional.isPresent()) {
            Cartao cartao = cartaoOptional.get();
            return cartao;
        } else {
            throw new RuntimeException("Cartao do cliente não foi encontrado");
        }

    }

    public Cartao buscarCartaoPeloId(int id) {
        Optional<Cartao> clienteOptional = cartaoRepository.findById(id);

        if (clienteOptional.isPresent()) {
            Cartao cartao = clienteOptional.get();
            return cartao;
        } else {
            throw new RuntimeException("Cartão não foi encontrado");
        }


    }
}
