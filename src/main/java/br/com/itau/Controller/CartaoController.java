package br.com.itau.Controller;


import br.com.itau.DTO.CartaoBloqueadoDTO;
import br.com.itau.DTO.CartaoDTO;
import br.com.itau.DTO.CartaoSaidaDTO;
import br.com.itau.DTO.ExtratoPagamentoDTO;
import br.com.itau.Model.Cartao;
import br.com.itau.Model.Pagamento;
import br.com.itau.Service.CartaoService;
import br.com.itau.Service.ClienteService;
import br.com.itau.Service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/cartoes")
public class CartaoController {


    @Autowired
    CartaoService cartaoService;

    @Autowired
    ClienteService clienteService;


    @Autowired
    PagamentoService pagamentoService;

    @PostMapping("/solicitarnovocartao")
    @ResponseStatus(HttpStatus.CREATED)
    public CartaoSaidaDTO solicitaNovoCartao(@RequestBody CartaoDTO cartaoDTO) {

        try {

            return cartaoService.solicitaNovoCartao(cartaoDTO);

        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }


    }


    //Buscar Cartão.
    @GetMapping(value = "/{numero}")
    @ResponseStatus(HttpStatus.OK)
    public Cartao buscarCartaoPeloNumero(@RequestBody @PathVariable(name = "numero") String numero) {
        try {
            return cartaoService.buscarCartaoPeloNumero(numero);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }

    //Ativa/Desativa
    @PutMapping(value = "/ativacartao/{numero}")
    @ResponseStatus(HttpStatus.OK)
    public Cartao ativaDesativaCartao(@RequestBody CartaoBloqueadoDTO status, @PathVariable(name = "numero") String numero) {
        try {

            Cartao obj = cartaoService.ativaCartaoCliente(numero, status.getStatuscartao());

            return obj;

        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }



}
