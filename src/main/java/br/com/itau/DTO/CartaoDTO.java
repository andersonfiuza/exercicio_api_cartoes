package br.com.itau.DTO;

import br.com.itau.Model.Cartao;

import javax.persistence.Id;

public class CartaoDTO {


    private int clienteId;

    private String numero;

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Cartao converterParaCartao() {
        Cartao cartao = new Cartao();

        cartao.setNumero(this.numero);

        return cartao;
    }


}
