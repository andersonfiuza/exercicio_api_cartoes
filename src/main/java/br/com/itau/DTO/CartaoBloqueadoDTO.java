package br.com.itau.DTO;

public class CartaoBloqueadoDTO {


    private Boolean statuscartao;

    public Boolean getStatuscartao() {
        return statuscartao;
    }

    public void setStatuscartao(Boolean statuscartao) {
        this.statuscartao = statuscartao;
    }
}
