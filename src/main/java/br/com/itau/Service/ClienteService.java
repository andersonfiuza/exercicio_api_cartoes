package br.com.itau.Service;


import br.com.itau.Model.Cliente;
import br.com.itau.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@Service
public class ClienteService {


    @Autowired
    ClienteRepository clienteRepository;

    public Cliente cadastrarnovocliente(Cliente cliente) {

        return clienteRepository.save(cliente);

    }

    public Cliente buscarClientePeloId(int id) {
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);

        if (clienteOptional.isPresent()) {
            Cliente cliente = clienteOptional.get();
            return cliente;
        } else {
            throw new RuntimeException("O cliente não foi encontrado");
        }


    }
}
