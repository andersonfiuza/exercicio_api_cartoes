package br.com.itau.DTO;

import br.com.itau.Model.Cartao;
import br.com.itau.Model.Pagamento;

import java.util.List;
import java.util.stream.Collectors;

public class ExtratoPagamentoDTO {

    private int id;
    private  int cartao_id;
    private  String descricao;
    private double valor;

    public int getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(int cartao_id) {
        this.cartao_id = cartao_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }


    public ExtratoPagamentoDTO (Pagamento pagamento){

    this.id = pagamento.getId();
    this.cartao_id = pagamento.getCartao().getId();
    this.descricao = pagamento.getDescricao();
    this.valor = pagamento.getValor();


    }

    public static  List<ExtratoPagamentoDTO> converter(List<Pagamento> pagamentos){

        return  pagamentos.stream().map(ExtratoPagamentoDTO::new).collect(Collectors.toList());

    }

}
