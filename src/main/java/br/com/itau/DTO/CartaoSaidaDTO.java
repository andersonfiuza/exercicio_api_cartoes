package br.com.itau.DTO;

import br.com.itau.Model.Cartao;

public class CartaoSaidaDTO {


    private int id;

    private int clienteId;

    private String numero;

    private Boolean ativo;




    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public CartaoSaidaDTO(Cartao cartao) {

        this.setAtivo(cartao.getAtivo());
        this.setClienteId(cartao.getCliente().getId());
        this.setNumero(cartao.getNumero());
        this.setId(cartao.getId());



    }

}
