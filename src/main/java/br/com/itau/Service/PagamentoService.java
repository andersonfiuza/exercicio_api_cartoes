package br.com.itau.Service;

import br.com.itau.DTO.ExtratoPagamentoDTO;
import br.com.itau.DTO.PagamentoEntradaDTO;
import br.com.itau.DTO.PagamentoSaidaDTO;
import br.com.itau.Model.Cartao;
import br.com.itau.Model.Pagamento;
import br.com.itau.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {


    @Autowired
    PagamentoRepository pagamentoRepository;

    @Autowired
    CartaoService cartaoService;

    public PagamentoSaidaDTO registraNovoPagamento(PagamentoEntradaDTO pagamentoEntradaDTO) {

        Cartao cartao = cartaoService.buscarCartaoPeloId(pagamentoEntradaDTO.getCartao_id());

        Pagamento pagamento = new Pagamento();

        pagamento.setCartao(cartao);
        pagamento.setDescricao(pagamentoEntradaDTO.getDescricao());
        pagamento.setValor(pagamentoEntradaDTO.getValor());


       pagamento = pagamentoRepository.save(pagamento);

        return new  PagamentoSaidaDTO(pagamento);

    }

    public List<Pagamento> lertodosospagamentos(int id) {


        return pagamentoRepository.findByCartaoId(id);



    }

}


