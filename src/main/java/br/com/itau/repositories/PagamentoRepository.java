package br.com.itau.repositories;

import br.com.itau.DTO.ExtratoPagamentoDTO;
import br.com.itau.Model.Pagamento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PagamentoRepository extends JpaRepository<Pagamento , Long> {


    List<Pagamento> findByCartaoId(int cartao_id);

}
